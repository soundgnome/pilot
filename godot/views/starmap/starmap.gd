extends Node2D

enum OBJECT_TYPE { STAR }

var _boundaries
var _cells
var _colors
var _hex_lengths
var _hex_size_vectors
var _offset

onready var _tileset = $stars.tile_set


func _ready():
	var hex_size = self._get_hex_size()

	self._boundaries = self._get_boundaries()
	self._hex_lengths = self._calculate_lengths(hex_size)
	self._offset = self._get_offset()
	self._hex_size_vectors = [
		Vector2(self._hex_lengths.half_width, self._hex_lengths.half_width),
		Vector2(self._hex_lengths.width, self._hex_lengths.width),
	]
	self._colors = self._get_colors()

	self._initialize_cells()
	self._load_objects()


func _add_cell_if_exists(cells, x, y):
	if x >= self._boundaries[0] and x < self._boundaries[2] \
		and y >= self._boundaries[0] and y < self._boundaries[3]:
		cells.append(self._cells[x][y])


func _add_gravity(pos, intensity, radius=0):
	for cell in self._get_cell_ring(pos, radius):
		cell.gravity += intensity
	if intensity > 1:
		self._add_gravity(pos, intensity-1, radius+1)


func _calculate_lengths(hex_size):
	var side_length = hex_size/sqrt(3)
	var lengths = {
		'width': hex_size,
		'half_width': hex_size/2,
		'side_length': int(side_length),
		'half_side': int(side_length/2),
		'row_height': int(side_length*1.5),
	}
	return lengths


func _draw():
	self._draw_objects()
	self._draw_grid()


func _draw_cell(cell):
	if cell.gravity > 0:
		self._draw_hex_background(cell.coords, self._colors.gravity[cell.gravity])
	for object in cell.objects:
		if object[0] == OBJECT_TYPE.STAR:
			var texture = self._tileset.tile_get_texture(object[1])
			var tile_region = self._tileset.tile_get_region(object[1])
			var target_rect = Rect2(cell.pos - self._hex_size_vectors[0], self._hex_size_vectors[1])
			self.draw_texture_rect_region(texture, target_rect, tile_region)


func _draw_grid():
	for x in range(self._boundaries[0], self._boundaries[2]):
		if (x == 0):
			for y in range(self._boundaries[1], self._boundaries[3]-1):
				self._draw_hex_border([x,y], [0,1,2,3,4])
			self._draw_hex_border([x, self._boundaries[3]-1], [0,1,2,3,4,5])
		else:
			for y in range(self._boundaries[1], self._boundaries[3]-1):
				self._draw_hex_border([x,y], [2,3,4])
			self._draw_hex_border([x, self._boundaries[3]-1], [2,3,4,5,0])


func _draw_hex_background(pos, color):
	var corners = self._get_hex_corners(pos)
	var vectors = PoolVector2Array(corners)
	var colors = PoolColorArray([
		color,
		color,
		color,
		color,
		color,
		color,
	])
	self.draw_polygon(vectors, colors, PoolVector2Array(), null, null, true)


func _draw_hex_border(pos, sides):
	var corners = self._get_hex_corners(pos)
	for side in sides:
		self.draw_line(corners[side], corners[(side+1)%6], self._colors.border, 1, true)


func _draw_objects():
	for x in range(self._boundaries[0], self._boundaries[2]):
		for y in range(self._boundaries[1], self._boundaries[3]):
			var pixel = self._pos_to_pixel([x,y])
			self._draw_cell(self._cells[x][y])


func _get_boundaries():
	return [0,0,32,32]


func _get_cell_ring(center, radius):
	if radius == 0:
		return [self._cells[center.x][center.y]]

	var cells = []
	for i in range(radius):
		self._add_cell_if_exists(cells, center.x-radius, center.y+i)
		self._add_cell_if_exists(cells, center.x-radius+i, center.y+radius)
		self._add_cell_if_exists(cells, center.x+i, center.y+radius-i)
		self._add_cell_if_exists(cells, center.x+radius, center.y-i)
		self._add_cell_if_exists(cells, center.x+radius-i, center.y-radius)
		self._add_cell_if_exists(cells, center.x-i, center.y-radius+i)
	return cells


func _get_colors():
	return {
		'border': Color(0.75, 0.75, 0.75),
		'gravity': {
			1: Color(0, 0, 0.25),
			2: Color(0, 0, 0.625),
			3: Color(0, 0, 1),
			4: Color(0, 0.25, 1),
			5: Color(0, 0.5, 1),
		},
	}


func _get_hex_corners(pos):
	var center = self._pos_to_pixel(pos)
	var x = [
		center[0] - self._hex_lengths.half_width,
		center[0],
		center[0] + self._hex_lengths.half_width,
	]
	var y = [
		center[1] - self._hex_lengths.side_length,
		center[1] - self._hex_lengths.half_side,
		center[1] + self._hex_lengths.half_side,
		center[1] + self._hex_lengths.side_length,
	]
	return [
		Vector2(x[1], y[3]),
		Vector2(x[0], y[2]),
		Vector2(x[0], y[1]),
		Vector2(x[1], y[0]),
		Vector2(x[2], y[1]),
		Vector2(x[2], y[2]),
	]


func _get_hex_size():
	return 24


func _get_offset():
	return Vector2(36,27)


func _initialize_cells():
	self._cells = []
	for x in range(self._boundaries[0], self._boundaries[2]):
		var row = []
		for y in range(self._boundaries[1], self._boundaries[3]):
			var coords = [x,y]
			var pixel = self._pos_to_pixel(coords)
			row.append({
				'gravity': 0,
				'objects': [],
				'coords': coords,
				'pos': Vector2(pixel[0], pixel[1]),
			})
		self._cells.append(row)


func _load_objects():
	var map = $stars
	for pos in map.get_used_cells():
		var type = map.get_cell(pos.x, pos.y)
		var cell = self._cells[pos.x][pos.y]
		cell.objects.append([OBJECT_TYPE.STAR, type])
		self._add_gravity(pos, type+2)


func _pos_to_pixel(pos):
	var x = (self._hex_lengths.width * (pos[0] - self._boundaries[0])) \
		+ (self._hex_lengths.half_width * pos[1]) + self._offset[0]
	var y = (self._hex_lengths.row_height * pos[1]) + self._offset[1]
	return [x, y]
